
function fontCheck(foo){
	if (foo.length > 6) {
		document.getElementById("label").style.left = "9.75em";
	} else if (foo.length < 2) {
		document.getElementById("label").style.left = "11em";
	} else {
		document.getElementById("label").style.left = "10em";
	}
}

function hireCost(base, seniority, tenure, competitiveness, managed) {
	var manageNum = ((Math.log2(managed.value + 8))/3);
	
	var cost = Math.round((base * .25) * (seniority.value *.4 + 1) * (tenure.value * .2 + 1) * (competitiveness.value * .2 + 1) * (manageNum));
	var cost_string =  (Math.round(cost)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	gauge.maxValue = base * 1.5
	document.getElementById("label").innerHTML = '$' + cost_string;
	fontCheck(cost_string);	
	
	gauge.set(cost);
	
	/*if (cost <= base * 1.5){
		gauge.set(cost);
		
	} else {
		gauge.set(base * 1.5);
	}*/
}
var opts = {
  angle: 0.35, // The span of the gauge arc
  lineWidth: 0.1, // The line thickness
  radiusScale: 1, // Relative radius
  pointer: {
    length: 0.6, // // Relative to gauge radius
    strokeWidth: 0.035, // The thickness
    //color: '#000000' // Fill color
  },
  limitMax: false,     // If false, max value increases automatically if value > maxValue
  limitMin: false,     // If true, the min value of the gauge will be fixed
  colorStart: '#19ff79',   // Colors
    colorMid: '#fff000',   // Colors
  colorStop: '#ff0000',    // just experiment with them
  strokeColor: '#EEEEEE',  // to see which ones work best for you
  generateGradient: true,
  //highDpiSupport: true,     // High resolution support
  percentColors: [[0.0, "#a9d70b" ], [0.50, "#f9c802"], [1.0, "#ff0000"]]

};

var target = document.getElementById('gauge'); // your canvas element
var gauge = new Donut(target).setOptions(opts); // create sexy gauge!
gauge.maxValue = 3000; // set max gauge value
gauge.setMinValue(0);  // Prefer setter over gauge.minValue = 0
gauge.animationSpeed = 84; // set animation speed (32 is default value)
gauge.set(0); // set actual value

var calc = document.getElementById('calc-btn');
calc.addEventListener('click', function() {
    console.log('btn clicked');
    var base = document.getElementById('base').value;
    var seniority  = document.getElementById('seniority');
    var tenure = document.getElementById('tenure');
    var competitiveness = document.getElementById('competitiveness');
    var managed = document.getElementById('managed');

    hireCost(base, seniority, tenure, competitiveness, managed);
}, false);
