
var hireGauge = (function() {

    function hireCost(base, seniority, tenure, competitiveness, managed) {
        var manageNum = ((Math.log2(managed + 8))/3);
        var cost = Math.round((base * .25) * (seniority *.4 + 1) * (tenure * .2 + 1) * (competitiveness * .2 + 1) * (manageNum));

        // console.info('manageNum: ', manageNum);
        // console.info('Managed: ', managed);
        // console.info('Base Final: ', base * .25);
        // console.info('Seniority Final: ', seniority *.4 + 1);
        // console.info('Tenure Final: ', tenure * .2 + 1);
        // console.info('Competitiveness Final: ', competitiveness * .2 + 1);
        // console.info('Cost: ', cost);

        if(checkNaN(cost)){
          return 0;
        } else if (cost <= (base * 1.5)){
            return cost;

        } else {
            return (base * 1.5);
        }
    }

    function getStats() {
        var base = parseInt(document.getElementById('base').value);
        var seniority = parseInt(document.getElementById('seniority').value);
        var tenure = parseInt(document.getElementById('tenure').value);
        var competitiveness = parseInt(document.getElementById('competitiveness').value);
        var managed = parseInt(document.getElementById('managed').value);

        return {
            base: base,
            sen: seniority,
            tenure: tenure,
            comp: competitiveness,
            managed: managed
        }
    }

    function getHireCost(e) {
        hideError();
        var stats = getStats();
        console.info('Stats: ', stats);
        checkValues(e);
        var cost = hireCost(stats.base,stats.sen,stats.tenure, stats.comp, stats.managed);
        checkNaN(cost);
        return cost;
    }

    function getInitial() {
        return 0;
    }

    function checkValues(e) {
        var stats = getStats();
        //console.log(stats);
        for(var k in stats) {
            //console.log(stats[k].length);
            if(stats[k].length == 0){
                displayError();
                throw new Error(k + ' ' + stats[k] + " is empty");
            }
        }

        return true;
    }

    function checkNaN(cost) {
        if(isNaN(cost)){
            displayError();
            throw new Error('Cost is NaN');
        }
        return false;
    }

    function displayError() {
        var errorDiv = document.getElementById('salary-guage-calc-error');
        errorDiv.innerHTML = "Please Complete All Fields";
        errorDiv.classList.remove('hidden');
    }

    function hideError() {
        var errorDiv = document.getElementById('salary-guage-calc-error');
        errorDiv.innerHTML = "Please Complete All Fields";
        errorDiv.classList.add('hidden');
    }

    var g = new JustGage({
        id: "gauge",
        title: "Max is 100.",
        value: getInitial(),
        min: 0,
        max: 200000,
        decimals: 0,
        gaugeWidthScale: 0.7
    });

    return {
        update: function(e) {
            var cost = getHireCost(e);
            g.refresh(cost);
            //g.refresh(g.originalValue,  1.2);
        },
        checkVals: function(e) {
           return checkValues(e);
        },
        new: function () {
           return g;
        }
    };
})();

// document.getElementById('guageForm').addEventListener('click', function(e) {
//     if(hireGauge.checkVals(e) === true){
//         document.getElementById('calc-btn').innerHTML = "Calculate";
//     }
// });

document.getElementById('calc-btn').addEventListener('click', function(e) {
    console.info('Guage Clicked');
    hireGauge.update(e);
});

document.addEventListener("DOMContentLoaded", function() {
    console.info('Guage Loaded');
    hireGauge.new();
});




